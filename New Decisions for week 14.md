# New Decisions for week 14

**1. Spliting the work for Exercise 1 and 2**

    We decided on a scheme for spliting the work between us:

        Different Information(Finn)

        MQTT(Mehrang)

        MQTT with MogoDB (Ulrik)

        MVP(Matas)

        OME(Mathias)

        Scrum roles(Bogdan)

        Sensors(Niks)

        Testplan(Victor)


**2. Backup plan for what we have to do**

    We have decided on who is gonna do the work for whom in case theres any problem and that person can't do it in time:

        Mathias will cover for Finn

        Ulrik will cover for Matas

        Mehrang will cover for Bogdan

        Victor will cover for Nikandras

        Bogdan will cover for Mathias

        Matas will cover for Victor

        Finn will cover for Ulrik

        Nikandras will cover for Mehrang

**3. Vote for how we are gonna do the report for OLA**

    We have made a vote to decide if are gonna work togheter on the report or just have a person do it.

        After the vote we decided doing it togheter in Google Docs.
